import './App.css';

import { Header } from './components/Header';
import { BrowserRouter as Router, Route } from 'react-router-dom';
import { Register } from './components/Register';
import SignIn from './components/LogIn';
import { Category } from './collections/Category';
import { CategoryEdit } from './collections/CategoryEdit';

function App() {
  return (
    <div className="App">
      <Router>
        <Route exact path='/' component={Header} />
        <Route exact path='/register' component={Register} />
        <Route exact path='/sign-in' component={SignIn} />
        <Route exact path='/category' component={Category} />
        <Route exact path='/category/:id' component={CategoryEdit} />
        
      </Router>
    </div>
  );
}

export default App;

