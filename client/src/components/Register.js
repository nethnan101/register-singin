import axios from 'axios';
import React, { useState, useContext } from 'react';
import { useHistory } from 'react-router-dom';
import { Context } from './Context';

export const Register = () => {

    const [name, setName] = useState('')
    const [phone, setPhone] = useState('')
    const [email, setEmail] = useState('')
    const [password, setPassword] = useState('')
    const history = useHistory()

    const  fetchUserByPhone= async (phone) => {
        // const res = await fetch(`http://localhost:8080/users?phone=${phone}`);
        // const data = await res.json();
        // return data;
        const res = await axios.get(process.env.REACT_APP_URL_API+`/auth?phone=${phone}`)
        .then(function (response) {
            return response.data;
        });
        // const data = await res.json();
        return res;
     
    }

    const  fetchUserByGmail= async (email) => {
        const res = await axios.get(process.env.REACT_APP_URL_API+`/auth?email=${email}`)
        .then(function (response) {
            return response.data;
        });
        return res;
    }

    const { dispatch } = useContext(Context);

    const onSubmitTask = async (e) => {

        e.preventDefault();
        if(!phone){
            alert('Please enter your phone number below');
            return;
        }
        if(!email){
            alert('Please enter your gmail address');
            return;
        }
        if(!password){
            alert('Please enter your passwork');
            return;
        }
        const createdPhone = await fetchUserByPhone(phone);
        if(createdPhone.length > 0){
            alert('This phone number is been used!');
            return;
        }
        const createdGmail = await fetchUserByGmail(email);
        if(createdGmail.length > 0){
            alert('This gmail is been used!')
            return;
        }

        var resultAddUser = await addNewUser({name, phone, email, password});

        setName('')
        setPhone('')
        setEmail('')
        setPassword('')

        if(resultAddUser){
            dispatch({type: "LOGIN_SUCESS", payload: resultAddUser});
            history.push('/');
        }
    }

    const addNewUser = async (datas) => {
        const res = await axios.post(process.env.REACT_APP_URL_API+'/auth/add', datas)
            .then(function (response) {
            return response.data;
        })
        return res;

        // const res = await axios.post(process.env.REACT_APP_URL_API+'/users', {
        //     method: 'POST',
        //     headers: {
        //         'Content-type': 'application/json'
        //     },
        //     body: JSON.stringify(datas)

        // })
        // const data = await res.json();
        // return data;

    }

    return (
        <div className='register'>
            <div className="container">
                <br />
                <h3>Please register the from below</h3>
                <form onSubmit={onSubmitTask}>
                    <div className="form-group">
                        <label htmlFor="name">Name:</label>
                        <input type="text" className="form-control" id="name" placeholder="Enter name" name="name" value={name} onChange={ (e) => setName(e.target.value) } />
                    </div>
                    <div className="form-group">
                        <label htmlFor="phone">Phone:</label>
                        <input type="text" className="form-control" id="phone" placeholder="Enter phone" name="phone" value={phone} onChange={ (e) => setPhone(e.target.value) } />
                    </div>
                    <div className="form-group">
                        <label htmlFor="email">Email:</label>
                        <input type="email" className="form-control" id="email" placeholder="Enter email" name="email" value={email} onChange={ (e) => setEmail(e.target.value) } />
                    </div>
                    <div className="form-group">
                        <label htmlFor="pwd">Password:</label>
                        <input type="password" className="form-control" id="pwd" placeholder="Enter password" name="pwd" value={password} onChange={ (e) => setPassword(e.target.value) } />
                    </div>

                    <button type="submit" className="btn btn-primary">Submit</button>
                </form>
            </div>
        </div>
    )
}
