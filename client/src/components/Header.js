import React from 'react';
import { Link } from 'react-router-dom';

import { useContext } from 'react';
// import { NavLink } from 'react-router-dom';
import { Context } from './Context';

// import { useState } from 'react';
// import { useHistory } from 'react-router-dom';


export const Header = () => {

    const {user, dispatch} = useContext(Context);

    const LogoutTask = () => {
        dispatch({ type: 'LOGOUT'});
    }

    // console.log('user Neth');
    // console.log(user);

    return (
        <div className='header'>
            <div className="">
                <nav className="navbar navbar-expand-sm navbar-dark bg-dark">
                    <div className="container container-fluid">
                        <Link className="navbar-brand" to='/'>Logo</Link>
                        <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#mynavbar">
                        <span className="navbar-toggler-icon"></span>
                        </button>
                        <div className="collapse navbar-collapse" id="mynavbar">
                            <ul className="navbar-nav me-auto">
                                <li className="nav-item">
                                    <Link className="nav-link" to='/'>Home</Link>
                                </li>
                                {user 
                                && (
                                    <>
                                        <li className="nav-item">
                                            <Link className="nav-link" to='/category'>Category</Link>
                                        </li>
                                    </>
                                    ) 
                                }
                                {user ? 
                                    (
                                        <>
                                            <li className="nav-item">
                                                <Link className="nav-link" to='/' onClick={LogoutTask}>LOG OUT</Link>
                                            </li>
                                            <li className="nav-item">
                                                <Link to='/' className="nav-link">{user.name}</Link>
                                            </li>
                                            <li className="nav-item">
                                                <Link to='/' className="nav-link">{user.phone}</Link>
                                            </li>
                                        </>
                                    ) : (
                                        <>
                                            <li className="nav-item">
                                                <Link className="nav-link" to='/register'>Register</Link>
                                            </li>
                                            <li className="nav-item">
                                                <Link className="nav-link" to='/sign-in'>Log In</Link>
                                            </li>
                                        </>
                                    )
                                }

                            </ul>
                            <form className="d-flex">
                                <input className="form-control me-2" type="text" placeholder="Search" />
                                <button className="btn btn-primary" type="button">Search</button>
                            </form>
                        </div>
                    </div>
                </nav>                           

            </div>
        </div>
    )
}
