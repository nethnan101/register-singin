import React, { useState, useContext, useEffect } from 'react'
import { Context } from '../components/Context'
import { Header } from '../components/Header'
import { useHistory } from 'react-router-dom';
import { Link } from 'react-router-dom';
import { useParams } from 'react-router';
import axios from 'axios';


export const CategoryEdit = () => {
    const [category, setCategory] = useState('')
    const [order, setOrder] = useState('')
    const [oldCategory, setoldCategory] = useState({});
    const { user } = useContext(Context);
    const history = useHistory();
    // const [auth, setAuth] = useState('');

    let { id } = useParams();

    const { dispatch } = useContext(Context);
    // const  fetchUserByGmail= async (email) => {
    //     const res = await axios.get(process.env.REACT_APP_URL_API+`/auth?email=${email}`)
    //     .then(function (response) {
    //         return response.data;
    //     });
    //     return res;
    // }
    
    // const getAuth = fetchUserByGmail()
    // console.log("GetAuth");
    // console.log(getAuth);
    
    const onSubmitTask = async (e) => {
        e.preventDefault();

        var resultCategory = await addCategory({category, order, userid: user._id, username: user.name})

        if(resultCategory){       
            history.push('/category');
        }

    }

    const addCategory = async (datas) => {
        const updateCate = { category: datas.category, order: datas.order };
        const res = await axios.post(process.env.REACT_APP_URL_API+`/category/update/${id}`, datas)
        .then((res) => {
            return res.data;
        });
        return res;
    }

    const getCategoryById = async (id) => {
        const res = await axios.get(process.env.REACT_APP_URL_API+`/category/${id}`)
            .then((res) => {
                return res.data;
            });
        return res;
    }
    useEffect (() => {
        const getCate = async (catid) => {
            const CategoryFormServer = await getCategoryById(catid)
            setCategory(CategoryFormServer.category)
            setOrder(CategoryFormServer.order)
            setoldCategory(CategoryFormServer[0]) 
        }
        getCate(id)
    },[id])

    return (
        <>  
            <Header />
            <div className="container">
                <h2>Edit</h2>
                <form onSubmit={onSubmitTask}>
                    <div className="form-group">
                        <label htmlFor="name">Name:</label>
                        <input type="text" className="form-control" id="category" placeholder="Enter Category" name="category" value={category} onChange={ (e) => setCategory(e.target.value) } />
                    </div>
                    <div className="form-group">
                        <label htmlFor="phone">Phone:</label>
                        <input type="text" className="form-control" id="order" placeholder="Enter product" name="order" value={order} onChange={ (e) => setOrder(e.target.value) } />
                    </div>
                    
                    <button type="submit" className="btn btn-primary">Update</button>
                    
                    <Link to='/category'>
                        <button className="btn btn-danger">Cencel</button>
                    </Link>
                </form>
            </div>
        </>
    )
}
