import React from 'react'
import { Link } from 'react-router-dom'
import '../App.css'

export const NameList = ({category, users, onDelete, onEdit}) => {

    return (
        <div>
            <br />
            <div className="container bg-success rounded p-2">
                <div className="row">
                    <div className="col-sm-6">
                        <h5>ID: </h5>
                        <h5>Category: </h5>
                        <h5>View Order: </h5>
                    </div>                 
                        {category.length < 0 ? (
                            <>
                                <div className="col-sm-4">
                                    <h5 className='text-light'>{category.id}</h5>
                                    <h5 className='text-light'>{category.category}</h5>
                                    <h5 className='text-light'>{category.order}</h5>
                                </div>
                                <div className="col-sm-2">
                                    <button className='cycle rounded' onClick={() => onDelete(category.id)}>Deleter</button>
                                    <Link to={'/category/' + category.id}>
                                        <button className='cycle rounded bg-danger'>Edit</button>
                                    </Link>    
                                </div>
                            </>
                        ): (
                            <h2>No Records</h2>
                        )}
                </div>
            </div>
        </div>
    )
}
