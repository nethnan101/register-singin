const express = require("express");
// categoryRoutes is an instance of the express router.
// We use it to define our routes.
// The router will be added as a middleware and will take control of requests starting with path /category.
const categoryRoutes = express.Router();
// This will help us connect to the database
const dbo = require("../db/conn");
// This help convert the id from string to ObjectId for the _id.
const ObjectId = require("mongodb").ObjectId;

// This section will help you get a list of all the category.
categoryRoutes.route("/category").get(function (req, res) {
	let db_connect = dbo.getDb("employees");
	db_connect
		.collection("category")
		.find( req.query )
		.toArray(function (err, result) {
		if (err) throw err;
			res.json(result);
	});
});

// This section will help you get a single category by id
categoryRoutes.route("/category/:id").get(function (req, res) {
	let db_connect = dbo.getDb();
	let myquery = { _id: ObjectId( req.params.id )};
	db_connect
		.collection("category")
		.findOne(myquery, function (err, result) {
			if (err) throw err;
			res.json(result);
	});
});

// This section will help you create a new category.
categoryRoutes.route("/category/add").post(function (req, response) {
	let db_connect = dbo.getDb();
	let myobj = {
		category: req.body.category,
		order: req.body.order,
		userid: req.body.userid,
		username: req.body.username
	};
	db_connect.collection("category").insertOne(myobj, function (err, res) {
		if (err) throw err;
		response.json(res);
	});
});

// This section will help you update a category by id.
categoryRoutes.route("/category/update/:id").post(function (req, response) {
	let db_connect = dbo.getDb();
	let myquery = { _id: ObjectId( req.params.id )};
	let newvalues = {
		$set: {
			category: req.body.category,
			order: req.body.order,
			userid: req.body.userid,
			username: req.body.username
		},
	};
	db_connect
		.collection("category")
		.updateOne(myquery, newvalues, function (err, res) {
		if (err) throw err;
		console.log("1 document updated");
		response.json(res);
		});
});

// This section will help you delete a category
categoryRoutes.route("/category/delete/:id").delete((req, response) => {
	let db_connect = dbo.getDb();
	let myquery = { _id: ObjectId( req.params.id )};
	db_connect.collection("category").deleteOne(myquery, function (err, obj) {
		if (err) throw err;
		console.log("1 document deleted");
		response.status(obj);
	});
});

module.exports = categoryRoutes;