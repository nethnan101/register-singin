import React,{ useState, useContext, useEffect } from 'react'
import { Context } from '../components/Context'
import { Link, useHistory } from 'react-router-dom'
import Swal from 'sweetalert2'
import axios from 'axios'

export const ListCategory = ({ EditCategory }) => {

    const [category, setCategory] = useState([])
    const { user } = useContext(Context);
    const history = useHistory();

    useEffect (() => {
        const fetchCategory = async (user) => {
            console.log("user");
            console.log(user);
            const resu = await axios.get(process.env.REACT_APP_URL_API+'/category', {
                params: {
                    userid: user._id
                }
            })
            .then(function (response) {
                return response.data;
            });
            setCategory(resu);
        }
        fetchCategory(user);        
    },[user])
    const deleteUsers = async (id) => {
        Swal.fire({
            title: 'Are you sure?',
            text: "You want to delete this data!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, delete it!'
        }).then( async (result) => {
            if (result.isConfirmed) {
                const result = await axios.delete(process.env.REACT_APP_URL_API+`/category/delete/${id}`)                         
            }
        })
    }

    return (
        <div>
            <div className="container">                    
                <table className="table">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Name</th>
                            <th>Order</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        {category.length > 0 ? (
                            <>
                                {category.map((category) => (
                                    <tr key={category._id}>
                                        <td>{category._id}</td>
                                        <td>{category.category}</td>
                                        <td>{category.order}</td>
                                        <td>
                                            <Link to={"/category/" + category._id } className="btn btn-primary m-1">Edit</Link>
                                            <Link to="/category" onClick={() => deleteUsers(category._id)} className="btn btn-danger m-1">Delete</Link>
                                        </td>                                
                                    </tr>

                                ))}
                            </>
                        ) : (
                            <>
                                <tr>
                                    <td colSpan="4">There are no record!</td>
                                </tr>
                            </>
                        ) }
                    </tbody>
                </table>
            </div>
        </div>
    )
}
