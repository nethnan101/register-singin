const express = require("express");
// authRoutes is an instance of the express router.
// We use it to define our routes.
// The router will be added as a middleware and will take control of requests starting with path /auth.
const authRoutes = express.Router();
// This will help us connect to the database
const dbo = require("../db/conn");
// This help convert the id from string to ObjectId for the _id.
const ObjectId = require("mongodb").ObjectId;

// This section will help you get a list of all the auth.
authRoutes.route("/auth").get(function (req, res) {
	let db_connect = dbo.getDb("auth");
	db_connect
		.collection("auth")
		.find( req.query )
		.toArray(function (err, result) {
		if (err) throw err;
			res.json(result);
		});
});

// This section will help you get a single auth by id
authRoutes.route("/auth/:id").get(function (req, res) {
	let db_connect = dbo.getDb();
	let myquery = { _id: ObjectId( req.params.id )};
	db_connect
		.collection("auth")
		.findOne(myquery, function (err, result) {
			if (err) throw err;
			res.json(result);
		});
});

// This section will help you create a new auth.
authRoutes.route("/auth/add").post(function (req, response) {
	let db_connect = dbo.getDb();
	let myobj = {
		name: req.body.name,
		phone: req.body.phone,
		email: req.body.email,
		password: req.body.password
	};
	db_connect.collection("auth").insertOne(myobj, function (err, res) {
		if (err) throw err;
		response.json(res);
	});
});

// This section will help you update a auth by id.
authRoutes.route("/auth/update/:id").post(function (req, response) {
	let db_connect = dbo.getDb();
	let myquery = { _id: ObjectId( req.params.id )};
	let newvalues = {
		$set: {
			name: req.body.name,
			phone: req.body.phone,
			email: req.body.email,
			password: req.body.password
		},
	};
	db_connect
		.collection("auth")
		.updateOne(myquery, newvalues, function (err, res) {
		if (err) throw err;
		console.log("1 document updated");
		response.json(res);
		});
});

// This section will help you delete a auth
authRoutes.route("/auth/delete/:id").delete((req, response) => {
	let db_connect = dbo.getDb();
	let myquery = { _id: ObjectId( req.params.id )};
	db_connect.collection("auth").deleteOne(myquery, function (err, obj) {
		if (err) throw err;
		console.log("1 document deleted");
		response.status(obj);
	});
});

module.exports = authRoutes;