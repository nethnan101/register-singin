import React, { useState, useContext } from 'react';
import { Context } from '../components/Context';
import { Header } from '../components/Header';
import { useHistory } from 'react-router-dom';
import axios from 'axios';

import { ListCategory } from './ListCategory';

export const Category = () => {
    const [category, setCategory] = useState('')
    const [order, setOrder] = useState('')
    // const [ oldcategory, setOldcategory] = useState([])
    const history = useHistory();
    
    const {user, dispatch } = useContext(Context);

    const onSubmitTask = async (e) => {
        e.preventDefault();
        if(!category){
            alert('Please enter category!');
            return;
        }
        if(!order){
            alert('Please enter products!');
            return;
        }
        
        var resultCategory = await addCategory({category, order, userid: user._id, username: user.name})
        
        setCategory('')
        setOrder('')

        if(resultCategory){
            history.push('/category');
        }
        
    }

    const addCategory = async (datas) => {
        const res = await axios.post(process.env.REACT_APP_URL_API+'/category/add', datas)
        .then(function (response){
            return response.data;
        })
        return res;
    }
    
    return (
        <>  
            <Header />
            <div className="container">
                <form onSubmit={onSubmitTask}>
                    <div className="form-group">
                        <label htmlFor="name">Name:</label>
                        <input type="text" className="form-control" id="category" placeholder="Enter Category" name="category" value={category} onChange={ (e) => setCategory(e.target.value) } />
                    </div>
                    <div className="form-group">
                        <label htmlFor="phone">Phone:</label>
                        <input type="text" className="form-control" id="order" placeholder="Enter product" name="order" value={order} onChange={ (e) => setOrder(e.target.value) } />
                    </div>
                    <button type="submit" className="btn btn-primary">Submit</button>
                </form>
            </div>
            <ListCategory />

        </>
    )
}
